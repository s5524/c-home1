﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace ZadDom
{
    class Program
    {
        private static Dictionary<int,Presence> _students = new Dictionary<int,Presence>();
        private static Course _course = new Course();
        private static int _am;
        private static int _pH;
        private static int _pW;
        private static int _idH = 1;


        static void Main(string[] args)
        {
            bool b = true;
            AddCourse();
            AddStudens();
            while (b)
            {
                Console.WriteLine("Podaj czynność 1:Dodaj obecnoćś 2:Doadaj zadanie domowe 3:Wypisz raport z kursu 4:Zakończ");
                string chose = Console.ReadLine();
                switch (chose)
                {
                    case "1":
                        AddPDay();
                        break;
                    case "2":
                        AddExam();
                        break;
                    case "3":
                        PrintD();
                        break;
                    case "4":
                        return;
                    default:
                        Console.WriteLine("błędna instrukcja");
                        break;
                }

            }
        }//maintry

        public static void AddCourse()
        {
            bool f = true;
            try
            {
                Course course = new Course();

                Console.WriteLine("Podaj dane kursu");
                Console.WriteLine("Podaj imię prowadzącego kurs");
                _course.ownerName = Console.ReadLine();
                Console.WriteLine("Podaj nazwę kursu ");
                _course.courseName = Console.ReadLine();
                do
                {
                    try
                    {
                        Console.WriteLine("Data rozpoczęcia kursu");
                        course.startingDate = DateTime.Parse(Console.ReadLine());
                        f = false;
                    }
                    catch (Exception e)
                    {
                        Console.WriteLine("zły format danych");
                    }
                } 
                while (f);

               f = true;

                    do
                    {
                        Console.WriteLine("Próg prac domowych 0-100");

                        _course.percentHomework = int.Parse(Console.ReadLine());
                        _pW = course.percentHomework;
                        if (_course.percentHomework < 0 || _course.percentHomework > 100)
                        {
                            Exception e;
                        }
                        else
                        {
                            f = false;
                        }
                    }
                    while (f);
                f = true;
                do
                {
                    
                    
                        Console.WriteLine("Próg obecności 0-100");
                        _course.percentPresence = Int32.Parse(Console.ReadLine());
                        _pH = course.percentPresence;
                        if (_course.percentPresence < 0 || _course.percentPresence > 100)
                        {
                            Console.WriteLine("Zły format danych-1-100");
                            Exception e;
                        }
                        else
                        {
                            f = false;
                        }
                    
                } while (f);
                f = true;
                do
                {
                    try
                    {
                        Console.WriteLine("Podaj liczbę kursantów");
                        course.courseMembers = Int32.Parse(Console.ReadLine());
                        _am = course.courseMembers;
                        f = false;
                    }
                    catch(Exception e)
                    {

                    }
                } while (f);
                    Console.WriteLine("Dodano kurs " + course.courseName);


            }
            catch (Exception e)
            {
                Console.WriteLine("Zły format danych");
            }


        }

        public static void AddStudens()
        {
            bool f = true;

            for (var i = 0; i < _am; i++)
            {
                Presence students = new Presence();
                try
                {
                    students.idS = Students.id;
                    Console.WriteLine("Podaj imie");
                    students.name = Console.ReadLine();
                    Console.WriteLine("Podaj nazwisko");

                    students.surname = Console.ReadLine();
                    Console.WriteLine("Podaj date urodzenia");
                    do
                    {
                        try
                        {
                            students.birthDate = DateTime.Parse(Console.ReadLine());
                            f = false;
                        }
                        catch (Exception e)
                        {
                            Console.WriteLine("żły format danych");
                        }
                    } while (f);
                    Console.WriteLine("podaj płeć M,K");
                    students.sex = Char.Parse(Console.ReadLine());
                    _students.Add(students.idS,students);
                    Students.id++;
                }
                catch
                {
                    Console.WriteLine("Zły format danych");
                }
            }
        }

        public static void AddPDay()
        {

            
            
                Console.WriteLine("Podaj dzien obecności");
                DateTime dateP = DateTime.Parse(Console.ReadLine());

            for (var i = 1; i < _students.Count + 1; i++)
            {
                PresenceD presence = new PresenceD();
                presence.studentID = i;

                Console.WriteLine("Czy student " + _students[i].name + " "+ _students[i].surname+" jest obecny 1:Tak inny znak nie ");
                var temp = Console.ReadLine();
                if (temp == "1")
                {
                    presence.presence = true;
                }
                else
                {
                    presence.presence = false;
                }

                _students[i].listPresences.Add(dateP, presence);
                   
            
            }
            

        }
        public static void AddExam()
        {
            bool f = true;
            int maxP = 0;
            try
            {

                Console.WriteLine("Podaj maksymalną liczbe punktów zadania domowego");
                maxP = int.Parse(Console.ReadLine());
            }
            catch
            {
                Console.WriteLine("Zły format danych");
            }

            for (var i = 1; i < _students.Count + 1; i++)
            {
                Homework homework = new Homework();
                homework.maxScore = maxP;
                

                Console.WriteLine("Jaką liczbę punktów otrzymał " + _students[i].name + " " + _students[i].surname);
                do
                {
                    homework.studentScore = int.Parse(Console.ReadLine());
                    if(homework.studentScore<0) 
                    {
                        Console.WriteLine("Błędne dane");
                        Exception e;
                    }
                    else if(homework.studentScore > homework.maxScore)
                    {
                        Console.WriteLine("Błędne dane");
                        Exception e;
                    }
                    else 
                    {
                    f = false;
                    }
                } while (f);
                f = true;
                _students[i].listHomewoork.Add(_idH, homework);
                _idH++;
            }

        }
        public static void PrintD()
        {

            double y;
            double x;
            double z;
            double a;
            Console.WriteLine("Nazwa kursu "+_course.courseName);
            Console.WriteLine("Nazwa rozpoczęcia kursu " + _course.startingDate);
            Console.WriteLine("Próg obecności " + _course.percentPresence +"%");
            Console.WriteLine("Próg zadań domowych " + _course.percentHomework + "%");


            foreach (var log in _students)
            {
                y = 0;
                x = 0;
                Console.WriteLine("Student " + log.Value.name);
                foreach (var dzien in log.Value.listPresences)
                {
                    y++;
                    if (dzien.Value.presence == true)
                    {
                        x++;
                    }

                }
                double c = (x / y) * 100;
                if (y > 0)
                {
                    if ((int)c < _pH)
                    {
                        Console.WriteLine(x + "/" + y + " " + "(" + (int)c + ")" + "% niezaliczone");
                    }
                    else
                    {
                        Console.WriteLine(x + "/" + y + " " + "(" + (int)c + ")" + "% zaliczone");
                    }
                }
            }
            foreach(var log in _students)
            {
                z = 0;
                a = 0;
                Console.WriteLine("Student " + log.Value.name + " " + log.Value.surname);
                
                foreach(var punkty in log.Value.listHomewoork)
                {
                    z += punkty.Value.studentScore;
                    a += punkty.Value.maxScore;
                }
                var d = (z / a)*100;
                if (a > 0)
                {
                    if (d >= _pW)
                    {

                        Console.WriteLine(z + "/" + a + " (" + (int)d + ")" + "%  zaliczone");
                    }
                    else
                    {
                        Console.WriteLine(z + "/" + a + " " + " (" + (int)d + ")" + "%  niezaliczone");

                    }
                }
            }

            

        }
    }

}
