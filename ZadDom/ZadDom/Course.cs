﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ZadDom
{
    class Course
    {
        public string courseName;
        public string ownerName;
        public DateTime startingDate;
        public int percentHomework;
        public int percentPresence;
        public int courseMembers;

    }
}
